MC-10 Technical Notes
=====================

Contents:
- MCU (CPU) Configuration
- Memory and I/O Maps
- Address Decoding
  - Expansion port `SEL` line usage
- Internal ROM
- Video


MCU (CPU) Configuration
-----------------------

The 6803 operates in mode 2:
- Internal RAM enabled (no internal ROM in this mode or in 6803)
- Port 3 is multiplexed address/data bus
- Port 4 is address bus
- SC1 is address strobe (`AS`)
- SC2 is read/write (`R/W̅`)
- Selected with P20 - P22 = %010 via diodes during reset)

Mode 2 memory map from datasheet:

    FFF0-FFFF   External Interrupt Vectors
    0100-FFEF   External Memory Space
    0080-00FF   Internal RAM
    0020-007F   External Memory Space
    0000-001F   Internal Registers

MCU Interrupt Vectors:

    FFFE    RESET
    FFFC    NMI
    FFFA    Software Interrupt (SWI)
    FFF8    IRQ1 (or IS3)
    FFF6    ICF (Input Capture)         ┐
    FFF4    OCF (Output Capture)        ├─ IRQ2 Interrupt
    FFF2    TOF (Timer Overflow)        │
    FFF0    SCI (RDRF+ORFE+TDRE)        ┘


Memory and I/O Maps
-------------------

Memory map (from Service Manual):

    E000    8K  BASIC ROM
    C000    8K  unused ROM area
    9000   12K  external I/O (keyboard, 6847 VDG)
    4000   20K  RAM (4K internal, 16K expansion)
    0400   15K  unused
    0100  768b  unused
    0000  256b  internal (6803) I/O

I/O map (`-`=unused, DDR=Data Direction Register):

    Adr Tandy Service Manual                    Motorola Datasheet
    ──────────────────────────────────────────────────────────────────────────
    7F  -                                       External Memory Space
    ... │                                       │
    20  -                                       External Memory Space
    ──────────────────────────────────────────────────────────────────────────
    1F  -                                       Reserved
    ... │                                       │
    15  -                                       Reserved
    14  RAM Control Register
    13  -                                       Transmit Data Register
    12  -                                       Receive Data Register
    11  -                                       Tx/Rx Control/Status Register
    10  -                                       Rate and Mode Control Register
    0F  Port 3 Control and Status Register
    0E  Input Capture Register LSB
    0D  Input Capture Register MSB
    0C  Output Compare Register LSB
    0B  Output Compare Register MSB
    0A  Counter LSB
    09  Counter MSB
    08  Timer Control and Status Register
    07  -                                       Port 4 Data Register
    06  -                                       Port 3 Data Register
    05  -                                       Port 4 DDR
    04  -                                       Port 3 DDR
    03  Miscellaneous I/O Data Register         Port 2 Data Register
    02  Keyboard Output Lines                   Port 1 Data Register
    01  DDR for miscellaneous I/O               Port 2 DDR
    00  DDR for keyboard lines                  Port 1 DDR


Address Decoding
----------------

Memory decoding is done by a [74LS155][SN74LS156N]
2× 2→4 decoder/demux with inverting outputs:
- Pins: `1C 1G̅ B 1Y̅3 1Y̅2 1Y̅1 1Y̅0 GND ‥ 2Y̅0 2Y̅1 2Y̅2 2Y̅3 A 2G̅ 2C̅ Vcc`
- `BA`: 00=xY0  01=xY1  10=xY3 11=xY4 (common for each half)
- `C` is 2nd select/enable for each half, assert both C and G to enable output
- 2× 2→4 demux: side 1 has pos/neg selects; side 2 has neg/neg

Configuration:
- Two halves, `1Yx` and `2Yx`.
  - Both halves disabled when `SEL=0` (default 1 due to pullup; see below)
  - `1Yx` enable also needs `E=1` (CPU half of bus cycle; other half is VDU)
  - `2Yx` eanble also needs `A12=0`, thus:
    - only `A15=0 A14=1 R/W̅=0` would enables internal RAM write for $4000-$7FFF
    - but because `A12=0` really just $4xxxx, $6xxx
- A15 and A14 are the B and A inputs (`BA` below), selecting outputs
  `xY0`-`xY3` on each half (when the half is enabled).

Decoding table:

    out  BA  R/W̅    enables
    ────────────────────────────────────────────────────────
    1Y0  00         N/C
    1Y1  01   x     '245 buffer connecting D0-D7 to on-board RAM
    1Y2  10   R     keyboard row inputs
              W     VDG control signals latch ('174 clock)
    1Y3  11   x     ROM
    ────────────────────────────────────────────────────────
    2Y0  00         N/C
    2Y1  01   W     pass write signal to internal RAM
    2Y2  10         N/C
    2Y3  11         N/C
    ────────────────────────────────────────────────────────

`A11` selects one internal RAM chip when low, other when high.
(Both chips get A0-A10.)

### Expansion port `SEL` line usage

- `SEL` has a pullup, high by default, enabling `1Yx`.
- Bringing `SEL=0` disables all internal systems decoding (`1Yx` and `2Yx`)
- External device must decode the addrs to which it responds and lower
  `SEL` when it's responding.

Standard 16K expansion cartridge:
- Not clear yet what range it decodes for itself.
- Probably $5000-$9FFF, given the memory map documented in Service Manual.

BIOS:
- Need to note what addrs it uses for external (to 6803; internal to MC-10)
  I/O; expansion devices that also select themselves in this range will
  break the BIOS.


Internal ROM
------------

According to the schematic, the ROM is an MB8364, which from [the picture
on this acution][kawai] is clearly a 24-pin chip. The "64" suffix would
seem to indicate that it's an 8K ROM, the schematic seems to match the
[2364 8K ROM pinout][mem], and `A12` is connected, addressing a full 8K.

Testing in an emulator seems to indicate that that $C000-$DFFF is a mirror
of the data in $E000-$FFFF; it's not clear if this is because the 8K ROM
was written that way or if it's secretly a 4K ROM inside that's ignoring
A12. But either way, it appears we can replace the ROM with a 2764 EPROM or
28C64 EEPROM (both have the same pinout) with our own code, or BASIC and
our own code.

The disassembly so far has only a few references to values between $C000
and $DFFF, and almost all of them look like fairly obviously bogus code
(probably the disassembler just has offset errors at those points because
I've not found all the data yet). Which makes sense, because there's no
reason for them to be using the mirrored copy of the ROM at $C000-$DFFF. So
probably $C000-$DFFF is entirely available for our own code without
breaking compatibility.

However, executing our own code with an unmodified BASIC ROM might be a bit
tricky; this BASIC has neither a `SYS` command nor a `USR()` function, as
far as I can tell.


Video
-----

The VDG is an MC6847, but only othe alphanumeric text mode and alpha
semigraphic-4 modes are supported. The screen codes are ASCII codes with
the lower case sticks producing inverted characters from the upper case
sticks.

The control signals are set with any write to $8000-$BFFF and all cleared
on reset.

    D2  GM2, I̅N̅T̅/EXT
    D3  GM1
    D4  GMD
    D5  A̅/G
    D6  CSS
    D7  R/F modulator

See the service manual for lots more mode information, but probably
still need to look at the 6845 datasheet, as well.


<!-------------------------------------------------------------------->
[SN74LS156N]: http://www.ti.com/product/sn74ls156/technicaldocuments
[kawai]: https://www.ebay.com/itm/283968832826
[mem]: https://github.com/0cjs/sedoc/blob/master/ee/memory.md#28-pin-device-pin-diagram

