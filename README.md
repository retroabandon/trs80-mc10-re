TRS-80 MC-10 (Micro Color Computer) Reverse Engineering
=======================================================

The Tandy TRS-80 MC-10 is also known as the Micro Color Computer. It uses a
.89 MHz 6800 CPU (actually a 6803) and is not compatible with the
6809-based Tandy Color Computer, though it does use the same Motorola 6847
video chip. It has 4K of RAM (expandable to 20K) and 8K of ROM containing
Microcolor BASIC v1.0

See [`techinfo/README`](./techinfo/README.md) for more technical details.

### Emulators

When using an emulator, the online [_Operation and Language Reference
Manual_][olr] may be handy.

- [Browser-based emulator][js-emu] ([GitHub][js-emu-git]).
  Greatly improved version of the JS one below?
- `colorcomputerarchive.com` [Root / MC-10 / Emulators][cca-emu].
  DCimcrocolor, Javascript Emulator and Virtual MC-10.

### ROM and Program Image Sources

- `colorcomputerarchive.com` [Root / MC-10][cca]. Subdirs include:
  - `Bios/`: ROM images; `Software/`: cassette images
  - `Hardware/` MC10 disk/joystick interface and MCX-128
  - `Utilties/`: PC programs including .cas/WAV/etc. converters.
  - `Documents/`: documentation, schematics, etc.
  Also documentation and emulators available.
- `theoldcomputer.com` [Tandy-Radio-Shack TRS-80-MC-10 ROMs][toc] folder



<!-------------------------------------------------------------------->
[cca-emu]: https://colorcomputerarchive.com/repo/MC-10/Emulators/
[cca]: https://colorcomputerarchive.com/repo/MC-10/
[js-emu-git]: https://github.com/mtinnes/mc-10
[js-emu]: http://faculty.cbu.ca/jgerrie/MC10/
[olr]: https://colorcomputerarchive.com/repo/MC-10/Documents/Manuals/Hardware/MC-10%20Operation%20and%20Language%20Reference%20Manual%20(Web)/
[t8c]: https://www.trs-80.com/wordpress/trs-80-computer-line/mc-10/
[toc]: https://www.theoldcomputer.com/roms/index.php?folder=Tandy-Radio-Shack/TRS-80-MC-10/Various
